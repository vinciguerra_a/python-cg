#!/usr/bin/env python
# Copyright (C) 2021 Alessandro Vinciguerra <vincigua@student.ethz.ch>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation (version 3).

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from numpy import sin, cos, pi, exp
from numpy.linalg import norm

MESH_SIZE_POWER = 3
MESH_SIZE = 2 ** MESH_SIZE_POWER
dx = 1 / MESH_SIZE

NO_GHOST = tuple([slice(1, -1)] * 3)

def setSize(N):
    global MESH_SIZE_POWER
    global MESH_SIZE
    global dx
    MESH_SIZE_POWER = N
    MESH_SIZE = 2 ** MESH_SIZE_POWER
    dx = 1 / MESH_SIZE

def innerProd(a, b):
    return np.sum(a[NO_GHOST] * b[NO_GHOST])

def steepestDescent(A, x, b, maxIt = 1000000, tol=1e-13, reset=50, retInfo = False):
    i = 0
    r = b - A(x)
    delta = innerProd(r, r)
    tol = tol * delta
    print(tol)
    while i < maxIt and delta > tol:
        q = A(r)
        alpha = delta / innerProd(r, q)
        x = x + alpha * r
        if i % reset == 0:
            r = b - A(x)
        else:
            r = r - alpha * q
        delta = innerProd(r, r)
        i = i + 1
    if retInfo:
        return x, i
    else:
        return x

def cg(A, x, b, maxIt = 1000000, tol = 1e-13, reset = 50, retInfo = False):
    i = 0
    r = b - A(x)
    d = r.copy()
    delta1 = innerProd(r, r)
    tol = tol * np.linalg.norm(b)

    while i < maxIt and delta1 > tol:
        q = A(d)
        alpha = delta1 / innerProd(d, q)
        x = x + alpha * d
        if i % reset == 0:
            r = b - A(x)
        else:
            r = r - alpha * q
        delta0 = delta1
        delta1 = innerProd(r, r)
        beta = delta1 / delta0
        d = r + beta * d
        i += 1
    if retInfo:
        return x, i
    else:
        return x

def zeroface(x):
    size = x.shape
    for face in range(6):
        d = face // 2
        if face & 1:
            src = size[d] - 2
            dst = src + 1
        else:
            src = 1
            dst = 0
        if d == 0:
            for j in range(1, size[1] - 1):
                for k in range(1, size[2] - 1):
                    x[dst, j, k] = 0
        elif d == 1:
            for i in range(1, size[0] - 1):
                for k in range(1, size[2] - 1):
                    x[i, dst, k] = 0
        elif d == 2:
            for i in range(1, size[0] - 1):
                for j in range(1, size[1] - 1):
                    x[i, j, dst] = 0
        else:
            raise Exception("Wrong face number for ZeroFace")

def periodicface(x):
    size = x.shape
    ext = np.array(size) - 1
    for face in range(6):
        d = face // 2
        N = size[d] - 1
        if d == 0:
            for j in range(1, ext[1]):
                for k in range(1, ext[2]):
                    # [0 + nghost - 1 - i, j, k] = [N - nghost - i, j, k]
                    x[0, j, k] = x[N - 1, j, k]
                    # [N - (nghost - 1) + i, j, k] = [0 + nghost + i, j, k]
                    x[N, j, k] = x[1, j, k]
        elif d == 1:
            for i in range(1, ext[0]):
                for k in range(1, ext[2]):
                    x[i, 0, k] = x[i, N - 1, k]
                    x[i, N, k] = x[i, 1, k]
        elif d == 2:
            for i in range(1, ext[1]):
                for j in range(1, ext[2]):
                    x[i, j, 0] = x[i, j, N - 1]
                    x[i, j, N] = x[i, j, 1]
        else:
            raise Exception("Wrong face number for PeriodicFace")

def laplace(x, bc=periodicface):
    bc(x)
    lap = x.copy()
    size = x.shape
    for i in range(1, size[0] - 1):
        for j in range(1, size[1] - 1):
            for k in range(1, size[2] - 1):
                lap[i,j,k] = dx ** -2 * (x[i + 1, j, k] + x[i, j + 1, k] + x[i, j, k + 1] \
                    + x[i - 1, j, k] + x[i, j - 1, k] + x[i, j, k - 1] \
                    - 6 * x[i, j, k])
    return lap

def runTest(N):
    if N != MESH_SIZE_POWER:
        setSize(N)
    size = tuple([MESH_SIZE + 2] * 3)
    rhs = np.zeros(size)
    solution = np.zeros(size)

    for i in range(1, MESH_SIZE + 1):
        for j in range(1, MESH_SIZE + 1):
            for k in range(1, MESH_SIZE + 1):
                x = (i - 0.5) * dx
                y = (j - 0.5) * dx
                z = (k - 0.5) * dx
                solution[i, j, k] = sin(2 * pi * x) * sin(2 * pi * y) * sin(2 * pi * z)
                rhs[i, j, k] = 12 * pi ** 2 * solution[i, j, k]

    lhs = np.zeros(size)

    lhs, it = cg(lambda x: -laplace(x), lhs, rhs, maxIt = 2000, retInfo=True, reset=1)
    print(MESH_SIZE_POWER,
            f"{norm((lhs - solution)[NO_GHOST]) / norm(solution[NO_GHOST]):.19f}",
            f"{norm((-laplace(lhs) - rhs)[NO_GHOST]) / norm(rhs[NO_GHOST]):.19f}",
            it, sep=',')

if __name__ == "__main__":
    for N in range(2, 8):
        runTest(N)
